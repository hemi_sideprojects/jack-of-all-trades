import "../styles/globals.css";
import type { AppProps } from "next/app";
import Navigation from "../components/navigation";
import Head from "next/head";

function MyApp({ Component, pageProps }: AppProps) {
	return (
		<div>
			<Head>
				<title>Jack of all Trades</title>
			</Head>
			<div className='flex flex-row'>
				<Navigation />
				<div className='container p-2'>
					<Component {...pageProps} />
				</div>
			</div>
		</div>
	);
}

export default MyApp;
