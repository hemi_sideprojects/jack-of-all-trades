import React from "react";
import Head from "next/head";
import Link from "next/link";

function Next() {
	return (
		<React.Fragment>
			<Head>
				<title>Next - Nextron (with-typescript)</title>
			</Head>
			<div>
				<div className='h-screen flex items-center justify-center bg-gray-200'>
					<h1 className='text-blue-500'>Hello Tailwind</h1>
				</div>
				<p>
					⚡ Electron + Next.js ⚡ -
					<Link href='/home'>
						<a>Go to home page</a>
					</Link>
				</p>
			</div>
		</React.Fragment>
	);
}

export default Next;
