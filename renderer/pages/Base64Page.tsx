import base64 from "base-64";
import { useState } from "react";

const Base64Page = () => {
	const [decoded, setDecoded] = useState("");
	const [encoded, setEncoded] = useState("");

	const decode = (input: string) => {
		setDecoded(input);
		setEncoded(base64.decode(input));
	};

	const encode = (input: string) => {
		setEncoded(input);
		setDecoded(base64.encode(input));
	};

	const clearInput = () => {
		setEncoded("");
		setDecoded("");
	};

	return (
		<div>
			<div className='flex flex-row'>
				<div className='flex-1 m-2'>
					<h2>Encoded</h2>
					<div className=' border-2 '>
						<textarea
							name=''
							id=''
							className='w-full'
							value={encoded}
							onChange={(change) => encode(change.target.value)}
						></textarea>
					</div>
				</div>
				<div className='flex-1 m-2'>
					<h2>Decoded</h2>
					<div className=' border-2 '>
						<textarea
							name=''
							id=''
							className='w-full'
							value={decoded}
							onChange={(change) => decode(change.target.value)}
						></textarea>
					</div>
				</div>
			</div>

			<button
				className='bg-gray-300 hover:bg-gray-400 w-full text-gray-800 font-bold py-2 px-4 rounded items-center'
				onClick={() => clearInput()}
			>
				<span>Clear Input</span>
			</button>
		</div>
	);
};

export default Base64Page;
