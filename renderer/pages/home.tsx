import React from "react";
import Link from "next/link";
function Home() {
	return (
		<div className='flex-1 p-3'>
			<div>⚡ Electron + Next.js ⚡ -</div>
			<Link href='/next'>
				<a>Go to next page</a>
			</Link>
		</div>
	);
}

export default Home;
