import dynamic from "next/dynamic";
import { useEffect, useState } from "react";
import json from "../../resources/items.json";
import CodeEditorWindow from "../components/CodeEditorWindow";

const javascriptDefault = "{}";
const testData = JSON.parse(`
[{
    "id": 1,
    "name": {
      "english": "Bulbasaur",
      "japanese": "フシギダネ",
      "chinese": "妙蛙种子",
      "french": "Bulbizarre"
    },
    "type": [
      "Grass",
      "Poison"
    ],
    "base": {
      "HP": 45,
      "Attack": 49,
      "Defense": 49,
      "Sp. Attack": 65,
      "Sp. Defense": 65,
      "Speed": 45
    }
  },
  {
    "id": 2,
    "name": {
      "english": "Ivysaur",
      "japanese": "フシギソウ",
      "chinese": "妙蛙草",
      "french": "Herbizarre"
    },
    "type": [
      "Grass",
      "Poison"
    ],
    "base": {
      "HP": 60,
      "Attack": 62,
      "Defense": 63,
      "Sp. Attack": 80,
      "Sp. Defense": 80,
      "Speed": 60
    }
  
}
]
  
  `);
const JsonMain = () => {
	const DynamicReactJson = dynamic(import("react-json-view"), { ssr: false });
	let data = testData;

	const [code, setCode] = useState(javascriptDefault);
	const [newData, setNewData] = useState([]);

	const handleCompile = () => {
		// We will come to the implementation later in the code
	};

	const checkStatus = async (token) => {
		// We will come to the implementation later in the code
	};

	// let test = {};
	// data.forEach((element) => {
	// 	if (!test[element.id]) {
	// 		test[element.id] = element;
	// 	}
	// });

	// console.log(test);

	return (
		<div className='flex-1'>
			<div>
				<p>Json Transform</p>
				<div className='border-2 w-full'>
					<textarea
						name=''
						id=''
						className='w-full'
						onChange={(value) => setCode(value.target.value)}
					></textarea>
				</div>
				<button onClick={() => setNewData(eval(code))}>Test</button>
			</div>
			<div className='flex flex-row'>
				<div className='flex-1 '>
					<div>
						<p>JSON Viewer</p>
					</div>
					<div
						className='border-2 p-1 overflow-y-auto '
						style={{ height: "80vh" }}
					>
						<DynamicReactJson src={data} />
					</div>
				</div>
				<div className='flex-1'>
					<div>
						<p>JSON new Obj</p>
					</div>
					<div
						className='border-2 p-1 overflow-y-auto '
						style={{ height: "80vh" }}
					>
						<DynamicReactJson src={newData} />
					</div>
				</div>
			</div>
		</div>
	);
};

export default JsonMain;
