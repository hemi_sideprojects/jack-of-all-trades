import Link from "next/link";

const Navigation = () => {
	return (
		<div className='flex-11 flex-col justify-between h-screen bg-white border-r'>
			<div className='px-4'>
				<nav className='flex flex-col mt-4 space-y-1'>
					<Link href='/home'>
						<a className='flex items-center px-4 py-2 text-gray-700 bg-gray-100 rounded-lg'>
							<svg
								xmlns='http://www.w3.org/2000/svg'
								className='w-5 h-5 opacity-75'
								fill='none'
								viewBox='0 0 24 24'
								stroke='currentColor'
								stroke-width='2'
							>
								<path
									stroke-linecap='round'
									stroke-linejoin='round'
									d='M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z'
								/>
								<path
									stroke-linecap='round'
									stroke-linejoin='round'
									d='M15 12a3 3 0 11-6 0 3 3 0 016 0z'
								/>
							</svg>

							<span className='ml-3 text-sm font-medium'>
								Dashboard
							</span>
						</a>
					</Link>
					<Link href='/JsonMain'>
						<a
							href='/JsonMain'
							className='flex items-center px-4 py-2 text-gray-700 bg-gray-100 rounded-lg'
						>
							<svg
								xmlns='http://www.w3.org/2000/svg'
								className='w-5 h-5 opacity-75'
								fill='none'
								viewBox='0 0 24 24'
								stroke='currentColor'
								stroke-width='2'
							>
								<path
									stroke-linecap='round'
									stroke-linejoin='round'
									d='M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z'
								/>
								<path
									stroke-linecap='round'
									stroke-linejoin='round'
									d='M15 12a3 3 0 11-6 0 3 3 0 016 0z'
								/>
							</svg>

							<span className='ml-3 text-sm font-medium'>
								JSON
							</span>
						</a>
					</Link>
					<Link href='/Base64Page'>
						<a
							href='/Base64Page'
							className='flex items-center px-4 py-2 text-gray-700 bg-gray-100 rounded-lg'
						>
							<svg
								xmlns='http://www.w3.org/2000/svg'
								className='w-5 h-5 opacity-75'
								fill='none'
								viewBox='0 0 24 24'
								stroke='currentColor'
								stroke-width='2'
							>
								<path
									stroke-linecap='round'
									stroke-linejoin='round'
									d='M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z'
								/>
								<path
									stroke-linecap='round'
									stroke-linejoin='round'
									d='M15 12a3 3 0 11-6 0 3 3 0 016 0z'
								/>
							</svg>

							<span className='ml-3 text-sm font-medium'>
								Base64
							</span>
						</a>
					</Link>
				</nav>
			</div>
		</div>
	);
};

export default Navigation;
