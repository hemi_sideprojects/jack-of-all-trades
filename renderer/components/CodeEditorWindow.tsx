import React, { useState } from "react";

import dynamic from "next/dynamic";
const MonacoEditor = dynamic(import("react-monaco-editor"), { ssr: false });

const code = `import React, { PureComponent } from 'react';
import MonacoEditor from '@uiw/react-monacoeditor';

export default class App extends PureComponent {
  render() {
    return (
      <MonacoEditor
        language="html"
        value="<h1>I ♥ react-codemirror2</h1>"
        options={{
          selectOnLineNumbers: true,
          roundedSelection: false,
          cursorStyle: 'line',
          automaticLayout: false,
          theme: 'vs-dark',
        }}
      />
    );
  }
}
`;

const CodeEditorWindow = () => {
	const [code, setCode] = useState(
		`function add(a, b) {\n  return a + b;\n}`
	);

	const onChange = (newValue, e) => {
		console.log("onChange", newValue, e);
	};
	const options = {
		selectOnLineNumbers: true,
		roundedSelection: false,
		readOnly: false,
		cursorStyle: "line",
		automaticLayout: false,
		theme: "vs-dark",
		scrollbar: {
			// Subtle shadows to the left & top. Defaults to true.
			useShadows: false,
			// Render vertical arrows. Defaults to false.
			verticalHasArrows: true,
			// Render horizontal arrows. Defaults to false.
			horizontalHasArrows: true,
			// Render vertical scrollbar.
			// Accepted values: 'auto', 'visible', 'hidden'.
			// Defaults to 'auto'
			vertical: "visible",
			// Render horizontal scrollbar.
			// Accepted values: 'auto', 'visible', 'hidden'.
			// Defaults to 'auto'
			horizontal: "visible",
			verticalScrollbarSize: 17,
			horizontalScrollbarSize: 17,
			arrowSize: 30,
		},
	};
	return (
		<div>
			<MonacoEditor
				height='500px'
				language='javascript'
				onChange={onChange.bind(this)}
				value={code}
			/>
		</div>
	);
};
export default CodeEditorWindow;
